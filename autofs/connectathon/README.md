# AutoFS Connectathon test suite
Basic tests for automounter AutoFS.

## How to run it

### Dependencies
Please refer to the top-leve README.md for common dependencies. Test-specific dependencies will automatically be installed when executing 'make run'.

### Execute the test
```bash
$ make run
```
